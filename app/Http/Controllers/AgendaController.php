<?php

namespace App\Http\Controllers;
use App\Citas;
use App\Eventos;
use App\Paciente;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Validator;
class AgendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showEvento($id)
    {
        $ev = Eventos::find($id);
        if(!empty($ev)){
          return view('Ajax.showEvento')->with('ev',$ev);
        }
        else{
         return response()->json(['error'=>'No se ha podido encontrar el evento']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateEvento(Request $request, $id)
    {
      $validate = Validator::make($request->all(), [
            'asunto' => 'required|max:255',
            'descripcion' => 'max:255',
            'fecha_inicio' => 'required|date',
            'fecha_fin' => 'required|date|after:fecha_inicio'
        ]);
        $Evento = Eventos::find($id);
      if ($validate->passes()) {
          try{
            $user = Auth::user();
            $Evento->fill($request->all());
            $Evento->save();
            return response()->json(['success'=>'Se a añadido el evento correctamente.','id'=>$Evento->id, 'asunto'=>$Evento->asunto, 'descripcion'=>$Evento->descripcion, 'inicio'=>$Evento->fecha_inicio, 'fin'=>$Evento->fecha_fin]);
          }
          catch(\Exception $e){
            return response()->json(['error'=>$e]);
          }
       }
       return response()->json(['error'=>$validate->errors()->all(), 'id'=>$Evento->id, 'asunto'=>$Evento->asunto, 'descripcion'=>$Evento->descripcion, 'inicio'=>$Evento->fecha_inicio, 'fin'=>$Evento->fecha_fin]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyEvento($id)
    {
          $Evento = Eventos::find($id);
          try{
            if($Evento->delete()){
              return response()->json(['success'=>'Se a borrado el evento correctamente.']);
            }
            return response()->json(['error'=>'No se pudo borrar el evento']);
          }
          catch(\Exception $e){
            return response()->json(['error'=>$e]);
          }
    }

    public function eventoStore(Request $request)
    {
      $validate = Validator::make($request->all(), [
            'asunto' => 'required|max:255',
            'descripcion' => 'max:255',
            'fecha_inicio' => 'required|date',
            'fecha_fin' => 'required|date|after:fecha_inicio'
        ]);
      if ($validate->passes()) {
          try{
            $user = Auth::user();
            $Evento = new Eventos;
            $Evento->fill($request->all());
            $Evento->id_usuario = $user->id;
            $Evento->save();
            return response()->json(['success'=>'Se a añadido el evento correctamente.', 'id' => $Evento->id, 'asunto'=>$Evento->asunto, 'descripcion'=>$Evento->descripcion, 'inicio'=>$Evento->fecha_inicio, 'fin'=>$Evento->fecha_fin]);
          }
          catch(\Exception $e){
            return response()->json(['error'=>$e]);
          }
       }
       return response()->json(['error'=>$validate->errors()->all()]);

    }
}
