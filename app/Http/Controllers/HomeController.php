<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $user = Auth::user();
      $hoy = new \DateTime('today');
      $hoy = date_format($hoy, 'Y-m-d H:i');
      $Agenda = $user->Agenda();
        return view('welcome')->with($Agenda)->with('hoy',$hoy);
    }
}
