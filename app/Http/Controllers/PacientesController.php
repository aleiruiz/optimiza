<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Paciente;
use App\Eventos;
use App\Nacionalidades;
use Flash;

class PacientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('Pacientes.index');
    }

    public function ajaxindex()
    {
        $Pacientes = Paciente::all();
        foreach($Pacientes as $paciente){
          $edad = new \DateTime($paciente->fecha_nacimiento);
          $hoy = new \Datetime();
          $edad = $hoy->diff($edad);
          $paciente->edad = $edad->y;
        }
        return $Pacientes->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          $Nacionalidades = Nacionalidades::get();
          return view('Pacientes.create')->with('Nacionalidades',$Nacionalidades);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $validate = $request->validate([
            'documento'        => 'required|file',
            'foto'             => 'image',
            'nombres'          => 'required|max:255',
            'apellido_paterno' => 'required|max:255',
            'apellido_materno' => 'required|max:255',
            'sexo'             => 'required',
            'fecha_nacimiento' => 'required|date',
            'correo'           => 'required|max:255',
            'id_documento'  =>  'required|unique:pacientes,id_documento,'
        ]);
        $paciente = new Paciente;
        $paciente->fill($request->all());
        $paciente->save();
        $imagen = 'default.jpg';
        if($request->file('foto') !== null){
          $brouchereTemporal = uniqid() . '.' .
          $request->file('foto')->getClientOriginalExtension();
          //obtenemos la extension original de nuestro archivo y la unimos a este nuevo alias, la foto queda guardada bajo este alias
          $imagen = ucwords($brouchereTemporal);
          $estructura = 'images/Pacientes/Foto/' . $paciente->id ;
          //definimos nuestra estructura con el nombre de nuestra empresa y el ID del Incidente
          $request->file('foto')->move($estructura, $brouchereTemporal);
        }
        $documento = '';
        if($request->file('documento') !== null){
          $brouchereTemporal = uniqid() . '.' .
          $request->file('documento')->getClientOriginalExtension();
          //obtenemos la extension original de nuestro archivo y la unimos a este nuevo alias, la documento queda guardada bajo este alias
          $documento = ucwords($brouchereTemporal);
          $estructura = 'images/Pacientes/Documento/' . $paciente->id ;
          //definimos nuestra estructura con el nombre de nuestra empresa y el ID del Incidente
          $request->file('documento')->move($estructura, $brouchereTemporal);
        }
        $paciente->foto = $imagen;
        $paciente->documento = $documento;
        $paciente->save();

        flash::success('Se ha guardado satisfactoriamente el paciente!');
        return redirect(route('Pacientes.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Nacionalidades = Nacionalidades::get();
        $paciente = Paciente::find($id);
        return view('Pacientes.edit')->with('paciente',$paciente)->with('Nacionalidades',$Nacionalidades);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $paciente = Paciente::find($id);
      $validate = $request->validate([
            'foto'             => 'image',
            'nombres'          => 'required|max:255',
            'apellido_paterno' => 'required|max:255',
            'apellido_materno' => 'required|max:255',
            'sexo'             => 'required',
            'fecha_nacimiento' => 'required|date',
            'correo'           => 'required|max:255',
            'id_documento'  =>  'required|unique:pacientes,id_documento,'.$paciente->id
        ]);
        $paciente->fill($request->all());
        $paciente->save();
        $imagen = $paciente->foto;
        if($request->file('foto') !== null){
          $brouchereTemporal = uniqid() . '.' .
          $request->file('foto')->getClientOriginalExtension();
          //obtenemos la extension original de nuestro archivo y la unimos a este nuevo alias, la foto queda guardada bajo este alias
          $imagen = ucwords($brouchereTemporal);
          $estructura = 'images/Pacientes/Foto/' . $paciente->id ;
          //definimos nuestra estructura con el nombre de nuestra empresa y el ID del Incidente
          $request->file('foto')->move($estructura, $brouchereTemporal);
        }
        $documento = $paciente->documento;
        if($request->file('documento') !== null){
          $brouchereTemporal = uniqid() . '.' .
          $request->file('documento')->getClientOriginalExtension();
          //obtenemos la extension original de nuestro archivo y la unimos a este nuevo alias, la documento queda guardada bajo este alias
          $documento = ucwords($brouchereTemporal);
          $estructura = 'images/Pacientes/Documento/' . $paciente->id ;
          //definimos nuestra estructura con el nombre de nuestra empresa y el ID del Incidente
          $request->file('documento')->move($estructura, $brouchereTemporal);
        }
        $paciente->foto = $imagen;
        $paciente->documento = $documento;
        $paciente->save();

        flash::success('Se ha guardado satisfactoriamente los datos del paciente!');
        return redirect(route('Pacientes.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Pacientes = Paciente::find($id);
        if(!empty($Pacientes)){
          $Pacientes->destroy($id);
          flash::success('Se ha borrado el registro correctamente');
          return redirect(route('Pacientes.index'));
        }
        flash::error('No se ha encontrado ningun registro con este id');
        return redirect(route('Pacientes.index'));
    }
}
