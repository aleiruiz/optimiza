<?php
namespace App;

use Illuminate\Database\Eloquent\Model as Model;

class RolesPermission extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'permission_role';
}
