<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Citas extends Model
{

  protected $fillable = ['descripcion', 'asunto', 'fecha_inicio', 'fecha_fin', 'id_usuario', 'id_paciente'];
}
