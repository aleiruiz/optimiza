<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use App\Citas;
use App\Eventos;
use App\Paciente;
use DB;

class User extends Authenticatable
{
    use Notifiable, EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function detachAllRoles()
    {
        DB::table('role_user')->where('user_id', $this->id)->delete();

        return $this;
    }

    /**
     * obtener la lista de eventos para este usuario
     *
     * @var array
     */
    public function agenda(){
      $Eventos = Eventos::where('id_usuario',$this->id)->get();
      $Citas = Citas::where('id_usuario',$this->id)->get();

      $data = array(
        'Eventos' => $Eventos,
        'Citas' => $Citas
      );
      return $data;
    }
}
