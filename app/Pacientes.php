<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    protected $fillable = [
    'documento',
    'id_documento',
    'numero_documento',
    'imagen',
    'nombres',
    'apellido_paterno',
    'apellido_materno',
    'sexo',
    'fecha_nacimiento',
    'foto',
    'nacionalidad_id',
    'movil',
    'fijo',
    'trabajo',
    'extension',
    'correo',
    'provincia',
    'ciudad',
    'sector',
    'direccion',
    'contacto_nom',
    'contacto_app',
    'contacto_apm',
    'contacto_movil',
    'contacto_fijo',
    'parentesco',
    'ars',
    'plan',
    'carnet',
    'afilado',
    'poliza',
    'nss',
    'escolaridad',
    'ocupacion',
    'estado_civil',
    'transfusion',
    'requerido'];
}
