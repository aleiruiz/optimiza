<?php

use Illuminate\Database\Seeder;
use App\User;

class permisos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('roles')->insert(
      [

        'name'             => 'Medico',
        'display_name'     => 'Medico',
        'description'      => 'Medico'

      ]
      );
      DB::table('permissions')->insert(
      [

        'name'             => 'Pacientes',
        'display_name'     => 'Administrar Pacientes',
        'description'      => 'Administra los pacientes'

      ]
      );
      DB::table('permission_role')->insert(
      [

        'permission_id' => 1,
        'role_id'       => 1

      ]
      );
      $user = User::find(1);
      $user->attachRole(1);
    }
}
