<?php

use Illuminate\Database\Seeder;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $hoy = new \DateTime('today');
      $mañana = new \DateTime('tomorrow');

      DB::table('users')->insert(
      [

      'name'              => 'Admin',
      'email'             => 'admin@admin.com',
      'password'          => bcrypt('123456')

      ]
      );
      DB::table('citas')->insert(
      [

      'descripcion'       => 'Prueba para Citas',
      'asunto'            => 'Cita para alei',
      'fecha_inicio'      => $hoy,
      'fecha_fin'         => $mañana,
      'id_usuario'        => 1,
      'id_paciente'       => 0

      ]
      );
      DB::table('eventos')->insert(
      [

        'descripcion'       => 'Prueba para Eventos',
        'asunto'            => 'Evento para alei',
        'fecha_inicio'      => $mañana,
        'fecha_fin'         => $mañana,
        'id_usuario'        => 1

      ]
      );
      $this->call(permisos::class);
      $this->call(nacionalidades::class);
    }
}
