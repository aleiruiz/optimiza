<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pacientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('documento');
            $table->string('id_documento')->default('000-0000000-0');
            $table->string('nombres');
            $table->string('apellido_paterno');
            $table->string('apellido_materno');
            $table->boolean('sexo');
            $table->date('fecha_nacimiento');
            $table->string('foto')->nullable();
            $table->integer('nacionalidad_id')->default(0);
            $table->string('movil')->nullable();
            $table->string('fijo')->nullable();
            $table->string('trabajo')->nullable();
            $table->string('extension')->nullable();
            $table->string('correo');
            $table->string('provincia')->nullable();
            $table->string('ciudad')->nullable();
            $table->string('sector')->nullable();
            $table->string('direccion')->nullable();
            $table->string('contacto_nom')->nullable();
            $table->string('contacto_app')->nullable();
            $table->string('contacto_apm')->nullable();
            $table->string('contacto_movil')->nullable();
            $table->string('contacto_fijo')->nullable();
            $table->string('parentesco')->nullable();
            $table->string('ars')->default('Privado');
            $table->integer('plan')->default(0);
            $table->integer('carnet')->nullable();
            $table->integer('afilado')->nullable();
            $table->integer('poliza')->nullable();
            $table->integer('nss')->nullable();
            $table->string('escolaridad')->nullable();
            $table->string('ocupacion')->nullable();
            $table->integer('estado_civil')->default(0);
            $table->boolean('transfusion')->default(0);
            $table->string('requerido')->nullable();
            $table->foreign('nacionalidad_id')->references('id')->on('nacionalidades');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pacientes');
    }
}
