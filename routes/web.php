<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();
Route::group(['middleware' => 'auth'], function () {

//Agenda
Route::resource('Agenda', 'AgendaController');
Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

//Pacientes
Route::resource('Pacientes', 'PacientesController');
Route::get('Pacientes/index/Ajax', 'PacientesController@ajaxindex')->name('Pacientes.ajax');
Route::get('Pacientes/Delete/{id}', 'PacientesController@destroy')->name('Pacientes.delete');

//Eventos
Route::post('Evento/Store', 'AgendaController@eventoStore')->name('Evento.store');
Route::get('Evento/{id}', 'AgendaController@showEvento')->name('Evento.show');
Route::post('Evento/{id}/Update', 'AgendaController@updateEvento')->name('Evento.update');
Route::post('Evento/{id}/Destroy', 'AgendaController@destroyEvento')->name('Evento.delete');

});
