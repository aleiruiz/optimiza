//== Class definition

var DropzoneDemo = function () {
    //== Private functions
    var demos = function () {
        // single file upload
        Dropzone.options.fotografia = {
            paramName: "Archivo", // The name that will be used to transfer the file
            maxFiles: 1,
            maxFilesize: 5, // MB
            addRemoveLinks: true,
            acceptedFiles: "image/*",
            accept: function(file, done) {
                if (file.name == "justinbieber.jpg") {
                    done("Naha, you don't.");
                } else {
                    done();
                }
            }
        };
    }

    return {
        // public functions
        init: function() {
            demos();
        }
    };
}();

DropzoneDemo.init();
