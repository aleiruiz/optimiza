//== Class definition

var Inputmask = function () {

    //== Private functions
    var demos = function () {


      // phone number format
      $("#cedula").inputmask("mask", {
          "mask": "999-9999999-9",

      });

      $("#movil").inputmask("mask", {
        "mask": "999-999-9999",
      
      });

      $("#fijo").inputmask("mask", {
        "mask": "999-999-9999",
      
      });

      $("#trabajo").inputmask("mask", {
        "mask": "999-999-9999",
      
      });



    }

    return {
        // public functions
        init: function() {
            demos();
        }
    };
}();

jQuery(document).ready(function() {
    Inputmask.init();
});
