//== Class definition

var DatatableRecordSelectionDemo = function() {
	//== Private functions

	var options = {
		// datasource definition
		data: {
			type: 'remote',
			source: {
				read: {
					// sample GET method
					method: 'GET',
					url: '../Pacientes/index/Ajax',
					map: function(raw) {
						// sample data mapping
						var dataSet = raw;
						if (typeof raw.data !== 'undefined') {
							dataSet = raw.data;
						}
						return dataSet;
					},
				},
			},
			},

		// layout definition
		layout: {
			theme: 'default', // datatable theme
			class: '', // custom wrapper class
			scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
			height: 550, // datatable's body's fixed height
			footer: false // display/hide footer
		},

		// column sorting
		sortable: true,

		pagination: true,

		// columns definition
		columns: [
			{
				field: 'id',
				title: '#',
				sortable: false,
				width: 40,
				textAlign: 'center',
				selector: {class: 'm-checkbox--solid m-checkbox--brand'}
			},
			{
				field: 'ID',
				title: 'ID',
				width: 40,
				template: '{{id}}',
			},
			{	field: 'foto',
				title: 'Foto',
				template: function(row) {
					if(row.foto == 'default.jpg'){
						return '<img src="../dropzone/upload.png"style="max-width: 100px">';
					}
					else{
						return '<img src="../images/Pacientes/Foto/'+row.id+'/'+row.foto+'"style="max-width: 100px">';
					}
				},
			},
			{
			 field: 'nombres',
			 title: 'Nombres',
			 width: 150,
			 // sortable: 'asc', // default sort
			 //filterable: false, // disable or enable filtering

			 // basic templating support for column rendering,
			 //template: '{{OrderID}} - {{ShipCountry}}',
		 }, {
			 field: 'apellido_paterno',
			 title: 'Apellido Paterno',
			 width: 150,
		 //  template: function(row) {
				 // callback function support for column rendering
			 //  return row.ShipCountry + ' - ' + row.ShipCity;
			 //},
		 }, {
			 field: 'apellido_materno',
			 title: 'Apellido Materno',
			 width: 150,
		 }, {
			 field: 'edad',
			 title: 'Edad'
		 },
			 {
				field: 'sexo',
				title: 'Sexo',
				template: function(row) {
					var status = {
						0: {'title': 'Hombre'},
						1: {'title': 'Mujer'},
					};
					return status[row.sexo].title;
				},
			},
				{
				field: 'Actions',
				width: 110,
				title: 'Actions',
				sortable: false,
				overflow: 'visible',
				template: function (row, index, datatable) {
					var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
					return '\
						\
						<a href="Pacientes/'+row.id+'/edit" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\
							<i class="la la-edit"></i>\
						</a>\
						<a href="Pacientes/Delete/'+row.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">\
							<i class="la la-trash"></i>\
						</a>\
					';
				},
			}],
	};


	var serverSelectorDemo = function() {

		// enable extension
		options.extensions = {
			checkbox: {},
		};
		options.search = {
			input: $('#PacienteSearch'),
		};

		var datatable = $('#PacientesDT').mDatatable(options);

		$('#pacientes_select').on('change', function() {
			datatable.search($(this).val().toLowerCase(), 'Status');
		});

		$('#pacientes_select1').on('change', function() {
			datatable.search($(this).val().toLowerCase(), 'Type');
		});


		$('#pacientes_select,#pacientes_select1').selectpicker();

		datatable.on('m-datatable--on-click-checkbox m-datatable--on-layout-updated', function(e) {
			// datatable.checkbox() access to extension methods

			var ids = datatable.getSelectedRecords();

			var count = ids.length;

			$('#pacientes_checkbox').html(count);
			if (count > 0 ) {
				$('#pacientes_actions').collapse('show');
			} else {
				$('#pacientes_actions').collapse('hide');
			}
		});

		$('#pacientes_fetch').on('show.bs.modal', function(e) {
			var ids = datatable.checkbox().getSelectedId();
			var c = document.createDocumentFragment();
			for (var i = 0; i < ids.length; i++) {
				var li = document.createElement('li');
				li.setAttribute('data-id', ids[i]);
				li.innerHTML = 'Selected record ID: ' + ids[i];
				c.appendChild(li);
			}
			$(e.target).find('.m_datatable_selected_ids').append(c);
		}).on('hide.bs.modal', function(e) {
			$(e.target).find('.m_datatable_selected_ids').empty();
		});

	};

	return {
		// public functions
		init: function() {
			serverSelectorDemo();
		},
	};
}();

jQuery(document).ready(function() {
	DatatableRecordSelectionDemo.init();
});
