<div class="modal-body">
  <div class="form-group m-form__group row m--margin-top-20">
    <label class="col-form-label col-lg-3 col-sm-12">Asunto</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
      <input type="text" class="form-control" name="asunto" data-z-index="1100" value="{{$ev->asunto}}" id="showAsunto" placeholder="Asunto"/>
    </div>
  </div>
  <div class="form-group m-form__group row m--margin-top-20">
    <label class="col-form-label col-lg-3 col-sm-12">Descripcion</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
      <input type="text" class="form-control" name="descripcion" data-z-index="1100" value="{{$ev->descripcion}}" id="showDesc"  placeholder="Descripcion breve"/>
    </div>
  </div>
  <div class="form-group m-form__group row m--margin-bottom-20">
   <label class="col-form-label col-lg-3 col-sm-12">Fecha y Hora de Inicio</label>
   <div class="col-lg-9 col-md-9 col-sm-12">
     <div class="input-group date"  data-z-index="1100">
       <input type="text" class="form-control m-input" name="fecha_inicio" readonly  value="{{$ev->fecha_inicio}}" id="m_datetimepicker_4_modal"/>
       <div class="input-group-append">
         <span class="input-group-text">
         <i class="la la-calendar-plus-o glyphicon-th"></i>
         </span>
       </div>
     </div>
   </div>
 </div>
  <div class="form-group m-form__group row m--margin-bottom-20">
    <label class="col-form-label col-lg-3 col-sm-12">Fecha y Hora de Fin</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
      <div class="input-group date"  data-z-index="1100">
        <input type="text" class="form-control m-input" readonly name="fecha_fin" value="{{$ev->fecha_fin}}" id="m_datetimepicker_5_modal"/>
        <div class="input-group-append">
          <span class="input-group-text">
          <i class="la la-calendar-plus-o glyphicon-th"></i>
          </span>
        </div>
      </div>
      <span class="m-form__help">Las fechas de inicio y fin deben ser validas para crear el registro</span>
    </div>
  </div>
  {!! Form::hidden('_token', Session::token(), ['class' => 'form-control']) !!}
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-danger m-btn" onclick="borrarEvento({{$ev->id}})">Borrar</button>
  <button type="button"class="btn btn-brand m-btn" data-dismiss="modal">Cerrar</button>
  <button type="button" class="btn btn-success m-btn" onclick="actualizarEvento({{$ev->id}})">Guardar</button>
</div>
<script type="text/javascript">
$('#m_datetimepicker_4_modal').datetimepicker({
    todayHighlight: true,
    autoclose: true,
    pickerPosition: 'bottom-left',
    todayBtn: true,
    format: 'yyyy-mm-dd hh:ii',
    lang: 'es'
});
$('#m_datetimepicker_5_modal').datetimepicker({
    todayHighlight: true,
    autoclose: true,
    pickerPosition: 'bottom-left',
    todayBtn: true,
    format: 'yyyy-mm-dd hh:ii',
    lang: 'es'
});
</script>
