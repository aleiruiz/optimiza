@extends('layouts.app')
@section('title')
Dashboard
@endsection
@section('content')
@include('ModalsIndex')


                    <!-- BEGIN: Subheader -->
										<div class="m-grid__item m-grid__item--fluid m-wrapper">
											<!-- BEGIN: Subheader -->
											<div class="m-subheader ">
												<div class="d-flex align-items-center">
													<div class="mr-auto">
														<h3 class="m-subheader__title m-subheader__title--separator">
															Agenda
														</h3>
														<ul class="m-subheader__breadcrumbs m-nav m-nav--inline" style="height: 37px">
															<li class="m-nav__item m-nav__item--home">
																<a href="#" class="m-nav__link m-nav__link--icon">
																	<i class="m-nav__link-icon la la-home"></i>
																</a>
															</li>
															<li class="m-nav__separator">
																-
															</li>
															<li class="m-nav__item">
																<a href="" class="m-nav__link">
																	<span class="m-nav__link-text">
																		Administracion
																	</span>
																</a>
															</li>
															<li class="m-nav__separator">
																-
															</li>
															<li class="m-nav__item">
																<a href="" class="m-nav__link">
																	<span class="m-nav__link-text">
																		Eventos
																	</span>
																</a>
															</li>
														</ul>
													</div>
													<div>

													</div>
												</div>
											</div>
											<!-- END: Subheader -->
											<div class="m-content">
												<div class="row">
													<div class="col-lg-12">
														<!--begin::Portlet-->
														<div class="m-portlet" id="m_portlet">
															<div class="m-portlet__head">
																<div class="m-portlet__head-caption">
																	<div class="m-portlet__head-title">
																		<span class="m-portlet__head-icon">
																			<i class="flaticon-map-location"></i>
																		</span>
																		<h3 class="m-portlet__head-text">
																			Agenda
																		</h3>
																	</div>
																</div>
																<div class="m-portlet__head-tools">
																	<ul class="m-portlet__nav">
																		<li class="m-portlet__nav-item">
																			<a href="#m_datetimepicker_modal" data-toggle="modal" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air">
																				<span>
																					<i class="la la-plus"></i>
																					<span>Nuevo</span>
																				</span>
																			</a>
																		</li>
																	</ul>
																</div>
															</div>
															<div class="m-portlet__body">
																<div id="calendario"></div>
															</div>
														</div>
														<!--end::Portlet-->
													</div>
												</div>
											</div>
										</div>

@endsection
@section('script')
<script src="{{ asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/demo/default/custom/components/calendar/basic.js') }}" type="text/javascript"></script>
@endsection
<script type="text/javascript">
var CalendarBasic = function() {

    return {
        //main function to initiate the module
        init: function() {
            var todayDate = moment().startOf('day');
            var YM = todayDate.format('YYYY-MM');
            var YESTERDAY = todayDate.clone().subtract(1, 'day').format('YYYY-MM-DD');
            var TODAY = todayDate.format('YYYY-MM-DD');
            var TOMORROW = todayDate.clone().add(1, 'day').format('YYYY-MM-DD');

            $('#calendario').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay,listWeek'
                },
                dayClick: function(date, jsEvent, view) {

									$('#m_datetimepicker_3_modal').val( date.format('YYYY-MM-DD'));
									$('#m_datetimepicker_2_modal').val( date.format('YYYY-MM-DD'));
									$('#m_datetimepicker_modal').modal('toggle');

                },
                eventClick: function(calEvent, jsEvent, view) {
                  // change the border color just for fun
                  $(this).css('border-color', 'red');
									var id = calEvent.id.split("_").pop();
									if(calEvent.tipo == "evento"){
										$.ajax({
					                     type: "get",
					                     url:  "{{ URL::to('Evento') }}" + "/" + id,
					                     data: { id: id },  // serializes the form's elements.
					                     success: function(data)
					                     {
																		 if($.isEmptyObject(data.error)){
																			 $('#formEvento').html(data);
																			 $('#showEvento').modal('toggle');
																		 }else{
																			 alert(data.error);
																		 }

					                     },
					                     error: function (xhr, ajaxOptions, thrownError) {
					                       alert("Ocurrio un error inesperado - Evento");
					                     }
					                   });
									}

                },

                editable: true,
                eventLimit: true, // allow "more" link when too many events
                navLinks: true,
                lang: 'es',
                events: [
                  @foreach($Eventos as $Evento)
                    {
											id: 'evento_{{$Evento->id}}',
											tipo: 'evento',
                      title: '{{$Evento->asunto}}',
                      start: moment('{{$Evento->fecha_inicio}}').format(),
                      description: '{{$Evento->descripcion}}',
                      end: moment('{{$Evento->fecha_fin}}').format(),
                      className: 'm-fc-event--light m-fc-event--solid-primary',
                      allDay : false
                    },
                  @endforeach
                  @foreach($Citas as $Cita)
                    {
											id: 'cita_{{$Cita->id}}',
                      title: '{{$Cita->asunto}}',
                      start: moment('{{$Cita->fecha_inicio}}').format(),
                      description: '{{$Cita->descripcion}}',
                      end: moment('{{$Cita->fecha_fin}}').format(),
                      className: 'm-fc-event--light m-fc-event--solid-warning',
                      allDay : false
                    },
                  @endforeach
                ],

                eventRender: function(event, element) {
                    if (element.hasClass('fc-day-grid-event')) {
                        element.data('content', event.description);
                        element.data('placement', 'top');
                        mApp.initPopover(element);
                    } else if (element.hasClass('fc-time-grid-event')) {
                        element.find('.fc-title').append('<div class="fc-description">' + event.description + '</div>');
                    } else if (element.find('.fc-list-item-title').lenght !== 0) {
                        element.find('.fc-list-item-title').append('<div class="fc-description">' + event.description + '</div>');
                    }
                }
            });
        }
    };
}();



</script>


<script type="text/javascript">

	function crearEvento()
	{
					$.ajax({
                     type: "post",
                     url:  "{{ URL::to('Evento/Store') }}",
                     data: $("#Evento").serialize(),  // serializes the form's elements.
                     success: function(data)
                     {
													 if($.isEmptyObject(data.error)){
														 alert(data.success);
														 var event={id: 'evento_'+data.id, title: data.asunto, start:  data.inicio, end: data.fin, description: data.descripcion, tipo: 'evento',  className: 'm-fc-event--light m-fc-event--solid-primary', allDay : false};
														 $('#calendario').fullCalendar( 'renderEvent', event, true);
														 $('#m_datetimepicker_modal').modal('toggle');
															printErrorMsg('');
													 }else{
														 printErrorMsg(data.error);
													 }

                     },
                     error: function (xhr, ajaxOptions, thrownError) {
                       alert("Ocurrio un error inesperado - Evento");
                     }
                   });
	}

	function borrarEvento(id)
	{
		var id_evento = 'evento_'+id;
					$.ajax({
										 type: "post",
										 url:  "{{ URL::to('Evento') }}" + "/" + id + "/Destroy",
										 data: $("#Evento").serialize(),  // serializes the form's elements.
										 success: function(data)
										 {
													 if($.isEmptyObject(data.error)){
														  alert(data.success);
															$('#calendario').fullCalendar( 'removeEvents', id_evento );
															$('#showEvento').modal('toggle');
															printErrorMsg('');
													 }else{
														 printErrorMsg(data.error);
													 }

										 },
										 error: function (xhr, ajaxOptions, thrownError) {
											 alert("Ocurrio un error inesperado - Evento");
										 }
									 });
	}

	function actualizarEvento(id)
	{
					var asunto      = $('#showAsunto').val();
					var descripcion = $('#showDesc').val();
					var inicio      = $('#m_datetimepicker_4').val();
					var fin 				= $('#m_datetimepicker_5').val();
							$.ajax({
                     type: "post",
                     url:  "{{ URL::to('Evento') }}" + "/" + id + "/Update",
                     data: $("#formEvento").serialize(),  // serializes the form's elements.
                     success: function(data)
                     {

													 if($.isEmptyObject(data.error)){
														 var original = $('#calendario').fullCalendar('clientEvents','evento_'+data.id)[0];
														 original.title = data.asunto;
														 original.start = data.inicio;
														 original.end	 	 = data.fin;
														 original.description = data.descripcion
														 $('#calendario').fullCalendar( 'updateEvent', original);
														 $('#showEvento').modal('toggle');
														 alert(data.success);
															printErrorMsg('');
													 }else{
														$('#showAsunto').val(asunto);
														$('#showDesc').val(descripcion);
														$('#m_datetimepicker_4').val(inicio);
														$('#m_datetimepicker_5').val(fin);
														 printErrorMsg(data.error);
													 }

                     },
                     error: function (xhr, ajaxOptions, thrownError) {
                       alert("Ocurrio un error inesperado - Evento");
                     }
                   });
	}

	function printErrorMsg (msg) {
			$(".print-error-msg").find("ul").html('');
			if(msg != ''){
				$(".print-error-msg").css('display','block');
				$.each( msg, function( key, value ) {
					$(".print-error-msg").find("ul").append('<li>'+value+'</li>');
				});
			}
			else{
				$(".print-error-msg").css('display','none');
			}
		}

</script>
