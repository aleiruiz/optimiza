<!-- begin::Footer -->
<footer class="m-grid__item		m-footer " style="max-height:100px">
  <div class="m-container m-container--fluid m-container--full-height m-page__container" style="max-height:100px">
    <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop" style="max-height:100px">
      <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last" style="max-height:100px">
        <span class="m-footer__copyright">
          2017 &copy; Administrador de Hospitales por <a href="https://keenthemes.com" class="m-link">Optymiza</a>
        </span>
      </div>
    </div>
  </div>
</footer>
<!-- end::Footer -->
