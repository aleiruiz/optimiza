<script src="{{ asset('assets/vendors/base/vendors.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/demo/default/base/scripts.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/app/js/dashboard.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/demo/default/custom/components/forms/widgets/select2.js') }}" type="text/javascript"></script>
<script src="{{ asset('dropzone/FileUploader.js') }}" type="text/javascript"></script>
<script type="text/javascript">
$( document ).ready(function() {
        new FileUploader('.uploader');
        });
        $('#m_datetimepicker_3_modal').datetimepicker({
            todayHighlight: true,
            autoclose: true,
            pickerPosition: 'bottom-left',
            todayBtn: true,
            format: 'yyyy/mm/dd hh:ii'
        });
        $('#m_datetimepicker_2_modal').datetimepicker({
            todayHighlight: true,
            autoclose: true,
            pickerPosition: 'bottom-left',
            format: 'yyyy/mm/dd hh:ii'
        });
</script>
