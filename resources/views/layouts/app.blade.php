<!DOCTYPE html>
<html lang="es" >
    <head>
        <meta charset="utf-8" />
        <title>
            @yield('title')
        </title>
        <meta name="description" content="Latest updates and statistic charts">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!--begin::Web font -->
        <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
        <script>
          WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
        </script>
        @include('layouts.partials.style')
        @yield('style')
    </head>
    <body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
  			<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">



  @include('layouts.partials.header')
  <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
  				<!-- BEGIN: Left Aside -->
  	@include('layouts.partials.side-menu')
    @yield('content')


</div>



  <!-- begin::Footer -->
  @include('layouts.partials.footer')
  <!-- end::Footer -->
</div>

  <!-- end:: Page -->



  @include('layouts.partials.scripts')
    @yield('script')
</body>
</html>
