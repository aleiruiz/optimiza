<!DOCTYPE html>
<html lang="en" >
    <head>
        <meta charset="utf-8" />
        <title>
           Login
        </title>
        <meta name="description" content="Latest updates and statistic charts">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!--begin::Web font -->
        <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
        <script>
          WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
        </script>
        <!--end::Web font -->
        <!--begin::Base Styles -->
        <link href="{{ asset('assets/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/demo/default/base/style.bundle.css') }}" rel="stylesheet" type="text/css" />
        <!--end::Base Styles -->
        <link rel="shortcut icon" href="{{ asset('assets/demo/default/media/img/logo/favicon.ico') }}" />
    </head>
    <!-- end::Head -->
    <!-- end::Body -->
    <body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
  		<!-- begin:: Page -->
  		<div class="m-grid m-grid--hor m-grid--root m-page">
  			<div class="m-login m-login--signin  m-login--5" id="m_login" style="background-image: url(../../../assets/app/media/img//bg/bg-3.jpg);">
  				<div class="m-login__wrapper-1 m-portlet-full-height">
  					<div class="m-login__wrapper-1-1">
  						<div class="m-login__contanier">
  							<div class="m-login__content">
  								<div class="m-login__logo">
  									<a href="#">
  										<img src="{{ asset('images/logo.png') }}" style="width:75%">
  									</a>
  								</div>
  								<div class="m-login__title">
  									<h3>
  										Contratalo!
  									</h3>
  								</div>
  								<div class="m-login__desc">
  									Soluciones tecnologicas para clinicas
  								</div>
  								<div class="m-login__form-action">
  									<button type="button" id="m_login_signup" class="btn btn-outline-focus m-btn--pill">
  										Crear una cuenta
  									</button>
  								</div>
  							</div>
  						</div>
  						<div class="m-login__border">
  							<div></div>
  						</div>
  					</div>
  				</div>
  				<div class="m-login__wrapper-2 m-portlet-full-height">
  					<div class="m-login__contanier">
  						<div class="m-login__signin">
  							<div class="m-login__head">
  								<h3 class="m-login__title">
  									Ingresar
  								</h3>
  							</div>
  							{!! Form::open(['route' => 'login', 'class' => 'm-login__form m-form']) !!}
  								{!! csrf_field() !!}
  								@include('common.errors')
  									<div class="form-group m-form__group">
    									<input class="form-control m-input" type="text" placeholder="Email" name="email" autocomplete="off">
    								</div>
    								<div class="form-group m-form__group">
    									<input class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password">
    								</div>

  									<div class="m-login__form-action">
  									<button class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">Ingresar</button>
  									</div>

  								{!! Form::close() !!}
  						</div>
  						<div class="m-login__signup">
  							<div class="m-login__head">
  								<h3 class="m-login__title">
  									Crear Cuenta
  								</h3>
  								<div class="m-login__desc">
  									Introduce tus datos aqui
  								</div>
  							</div>
  							<form class="m-login__form m-form" action="">
  								<div class="form-group m-form__group">
  									<input class="form-control m-input" type="text" placeholder="Nombre" name="name">
  								</div>
  								<div class="form-group m-form__group">
  									<input class="form-control m-input" type="text" placeholder="Email" name="email" autocomplete="off">
  								</div>
  								<div class="form-group m-form__group">
  									<input class="form-control m-input" type="password" placeholder="Password" name="password">
  								</div>
  								<div class="form-group m-form__group">
  									<input class="form-control m-input m-login__form-input--last" type="password" placeholder="Confirmar password" name="rpassword">
  								</div>
  								<div class="m-login__form-sub">
  									<label class="m-checkbox m-checkbox--focus">
  										<input type="checkbox" name="agree">
  										I Agree the
  										<a href="#" class="m-link m-link--focus">
  											terms and conditions
  										</a>
  										.
  										<span></span>
  									</label>
  									<span class="m-form__help"></span>
  								</div>
  								<div class="m-login__form-action">
  									<button id="m_login_signup_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
  										Sign Up
  									</button>
  									<button id="m_login_signup_cancel" class="btn btn-outline-focus m-btn m-btn--pill m-btn--custom">
  										Cancel
  									</button>
  								</div>
  							</form>
  						</div>
  						<div class="m-login__forget-password">
  							<div class="m-login__head">
  								<h3 class="m-login__title">
  									Forgotten Password ?
  								</h3>
  								<div class="m-login__desc">
  									Enter your email to reset your password:
  								</div>
  							</div>
  							<form class="m-login__form m-form" action="">
  								<div class="form-group m-form__group">
  									<input class="form-control m-input" type="text" placeholder="Email" name="email" id="m_email" autocomplete="off">
  								</div>
  								<div class="m-login__form-action">
  									<button id="m_login_forget_password_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
  										Request
  									</button>
  									<button id="m_login_forget_password_cancel" class="btn btn-outline-focus m-btn m-btn--pill m-btn--custom ">
  										Cancel
  									</button>
  								</div>
  							</form>
  						</div>
  					</div>
  				</div>
  			</div>
  		</div>
  		<!-- end:: Page -->
        <script src="{{ asset('assets/vendors/base/vendors.bundle.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/demo/default/base/scripts.bundle.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/snippets/pages/user/login.js') }}" type="text/javascript"></script>
    </body>
</html>
