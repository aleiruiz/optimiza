@extends('layouts.app')
@section('title')
Pacientes Index
@endsection
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <!-- BEGIN: Subheader -->
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title m-subheader__title--separator">
          Pacientes
        </h3>
        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
          <li class="m-nav__item m-nav__item--home">
            <a href="#" class="m-nav__link m-nav__link--icon">
              <i class="m-nav__link-icon la la-home"></i>
            </a>
          </li>
          <li class="m-nav__separator">
            -
          </li>
          <li class="m-nav__item">
            <a href="" class="m-nav__link">
              <span class="m-nav__link-text">
                Registro de Pacientes e Historia Medica
              </span>
            </a>
          </li>

        </ul>
      </div>

    </div>
  </div>
  <!-- END: Subheader -->
  <div class="m-content">
    
    @include('flash::message')

    <div class="m-portlet m-portlet--mobile">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">
              Pacientes
            </h3>
          </div>
        </div>
        <div class="m-portlet__head-tools">

        </div>
      </div>
      <div class="m-portlet__body">
        <!--begin: Search Form -->
        <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
          <div class="row align-items-center">
            <div class="col-xl-8 order-2 order-xl-1">
              <div class="form-group m-form__group row align-items-center">
                <div class="col-md-4">
                  <div class="m-form__group m-form__group--inline">
                    <div class="m-form__label">
                      <label>
                        Status:
                      </label>
                    </div>
                    <div class="m-form__control">
                      <select class="form-control m-bootstrap-select m-bootstrap-select--solid" id="pacientes_select">
                        <option value="">
                          All
                        </option>
                        <option value="1">
                          Pending
                        </option>
                        <option value="2">
                          Delivered
                        </option>
                        <option value="3">
                          Canceled
                        </option>
                      </select>
                    </div>
                  </div>
                  <div class="d-md-none m--margin-bottom-10"></div>
                </div>
                <div class="col-md-4">
                  <div class="m-form__group m-form__group--inline">
                    <div class="m-form__label">
                      <label class="m-label m-label--single">
                        Type:
                      </label>
                    </div>
                    <div class="m-form__control">
                      <select class="form-control m-bootstrap-select m-bootstrap-select--solid" id="pacientes_select">
                        <option value="">
                          All
                        </option>
                        <option value="1">
                          Online
                        </option>
                        <option value="2">
                          Retail
                        </option>
                        <option value="3">
                          Direct
                        </option>
                      </select>
                    </div>
                  </div>
                  <div class="d-md-none m--margin-bottom-10"></div>
                </div>
                <div class="col-md-4">
                  <div class="m-input-icon m-input-icon--left">
                    <input type="text" class="form-control m-input m-input--solid" placeholder="Buscar..." id="PacienteSearch">
                    <span class="m-input-icon__icon m-input-icon__icon--left">
                      <span>
                        <i class="la la-search"></i>
                      </span>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-4 order-1 order-xl-2 m--align-right">
              <a href="{{Route('Pacientes.create')}}" class="btn btn-info m-btn m-btn--custom m-btn--icon m-btn--air">
                <span>
                  <i class="la la-user"></i>
                  <span>
                    Nuevo Paciente
                  </span>
                </span>
              </a>
              <div class="m-separator m-separator--dashed d-xl-none"></div>
            </div>
          </div>
        </div>
        <!--end: Search Form -->
<!--begin: Selected Rows Group Action Form -->
        <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30 collapse" id="pacientes_actions">
          <div class="row align-items-center">
            <div class="col-xl-12">
              <div class="m-form__group m-form__group--inline">
                <div class="m-form__label m-form__label-no-wrap">
                  <label class="m--font-bold m--font-danger-">
                    Selected
                    <span id="pacientes_checkbox"></span>
                    records:
                  </label>
                </div>
                <div class="m-form__control">
                  <div class="btn-toolbar">
                    <div class="dropdown">
                      <button type="button" class="btn btn-accent btn-sm dropdown-toggle" data-toggle="dropdown">
                        Update status
                      </button>
                      <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                        <a class="dropdown-item" href="#">
                          Pending
                        </a>
                        <a class="dropdown-item" href="#">
                          Delivered
                        </a>
                        <a class="dropdown-item" href="#">
                          Canceled
                        </a>
                      </div>
                    </div>
                    &nbsp;&nbsp;&nbsp;
                    <button class="btn btn-sm btn-danger" type="button" id="m_datatable_delete_all1">
                      Delete All
                    </button>
                    &nbsp;&nbsp;&nbsp;
                    <button class="btn btn-sm btn-success" type="button" data-toggle="modal" data-target="#pacientes_fetch">
                      Fetch Selected Records
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!--end: Selected Rows Group Action Form -->
<!--begin: Datatable -->
        <div class="m_datatable" id="PacientesDT"></div>
        <!--end: Datatable -->
<!--begin::Modal-->
        <div class="modal fade" id="pacientes_fetch" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                  Modal title
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">
                    &times;
                  </span>
                </button>
              </div>
              <div class="modal-body">
                <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-max-height="200">
                  <ul class="m_datatable_selected_ids"></ul>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                  Close
                </button>
              </div>
            </div>
          </div>
        </div>
        <!--end::Modal-->

      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script src="{{asset('datatables/pacientesDT.js')}}" type="text/javascript"></script>
@endsection
