@extends('layouts.app')
@section('title')
Registrar Paciente
@endsection
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <!-- BEGIN: Subheader -->
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title m-subheader__title--separator">
          Pacientes
        </h3>
        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
          <li class="m-nav__item m-nav__item--home">
            <a href="#" class="m-nav__link m-nav__link--icon">
              <i class="fa fa-user"></i>
            </a>
          </li>
          <li class="m-nav__separator">
            -
          </li>
          <li class="m-nav__item">
            <a href="" class="m-nav__link">
              <span class="m-nav__link-text">
                Pacientes
              </span>
            </a>
          </li>
          <li class="m-nav__separator">
            -
          </li>
          <li class="m-nav__item">
            <a href="" class="m-nav__link">
              <span class="m-nav__link-text">
                Registro de Pacientes
              </span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
  @include('common.errors')
  <!-- END: Subheader -->
  <div class="m-content">
    <div class="row">
      <div class="col-lg-12">
        <!--begin::Portlet-->
        <div class="m-portlet">
            {!! Form::model($paciente, ['route' => ['Pacientes.update',$paciente], 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed', 'method'=>'PUT', 'files' => 'true']) !!}
          <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
              <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon m--hide">
                  <i class="la la-gear"></i>
                </span>
                <h3 class="m-portlet__head-text">
                  Datos Generales
                </h3>
              </div>
            </div>
          </div>
          <!--begin::Form-->

            <div class="m-portlet__body">
            <div class="form-group m-form__group row">
                <div class="form-group m-form__group row col-md-6">
                  <label class="uploader loaded" ondragover="return false">
                     <i class="icon-upload icon"></i>
                     <img src="{{ asset('images/Pacientes/Foto/'.$paciente->id.'/') }}/{{$paciente->foto}}" class="loaded" style="z-index: 10">
                     <input type="file" accept="image/*" name="foto">
                 </label>
                </div>
                <div class="form-group m-form__group row col-md-6">
                  <label>
                    <a href="{{ asset('images/Pacientes/Documento/'.$paciente->id.'/')}}/{{$paciente->documento}}" download>Descargar Documento.. Identificacion</a>

                  </label>
                  <div class="col-lg-12"></div>
                    <label for="files" class="btn">
                      Remplazar documento:
                    </label>
                    <input type="file" id="files" class="form-control m-input" accept="application/pdf" name="documento" value="{{$paciente->file}}">
                    <span class="m-form__help">
                      Documentos aceptados(Cedula, Pasaporte, Menor de Edad), recuerde utilizar archivos pdf para esto*
                    </span>
                </div>
            </div>
              <div class="form-group m-form__group row">
                <div class="col-lg-6">
                  <label>
                    Nombre(s):
                  </label>
                  <input type="text" name="nombres" value="{{$paciente->nombres}}" class="form-control m-input" placeholder="Introduce Nombre">
                  <span class="m-form__help">
                    Introduce los nombres
                  </span>
                </div>
                <div class="col-lg-6">
                  <label>
                    Cedula:
                  </label>
                  <input type="text" name="id_documento" value="{{$paciente->id_documento}}" class="form-control m-input" id="cedula" placeholder="Introduce Cedula">
                  <span class="m-form__help">
                    Introduce una cedula unica
                  </span>
                </div>
                <div class="col-lg-6">
                  <label>
                    Fecha de Nacimiento:
                  </label>
                  <div class="input-group date">
                        <input type="text" class="form-control m-input" value="{{$paciente->fecha_nacimiento}}" name="fecha_nacimiento" readonly  placeholder="Selecciona Fecha de Nacimiento" id="m_datepicker_2"/>
                        <div class="input-group-append">
                          <span class="input-group-text">
                            <i class="la la-calendar-check-o"></i>
                          </span>
                        </div>
                    </div>
                  <span class="m-form__help">
                    Porfavor selecciona la fecha de nacimiento
                  </span>
                </div>

              </div>
              <div class="form-group m-form__group row">
                <div class="col-lg-6">
                  <label class="">
                    Apellido Paterno:
                  </label>
                  <input type="text" name="apellido_paterno" class="form-control m-input" value="{{$paciente->apellido_paterno}}"placeholder="introduce apellido paterno">
                  <span class="m-form__help">
                    Introduce apellido paterno
                  </span>
                </div>
                <div class="col-lg-6">
                  <label class="">
                    Apellido Materno:
                  </label>
                  <input type="text" name="apellido_materno" class="form-control m-input" value="{{$paciente->apellido_materno}}" placeholder="introduce apellido materno">
                  <span class="m-form__help">
                    Introduce apellido materno
                  </span>
                </div>
              </div>
              <div class="form-group m-form__group row">
                <div class="col-lg-6">
                  <label>
                    Sexo:
                  </label>
                  <div class="m-radio-inline">
                    <label class="m-radio m-radio--solid">
                      @if($paciente->sexo == 0)
                      <input type="radio" name="sexo" checked  value="0">
                      @else
                      <input type="radio" name="sexo"  value="0">
                      @endif
                      Hombre
                      <span></span>
                    </label>
                    <label class="m-radio m-radio--solid">
                      @if($paciente->sexo == 1)
                      <input type="radio" name="sexo" checked  value="1">
                      @else
                      <input type="radio" name="sexo"  value="1">
                      @endif
                      Mujer
                      <span></span>
                    </label>
                  </div>
                  <span class="m-form__help">
                    Selecciona el sexo del paciente
                  </span>
                </div>
                <div class="col-lg-6">
                  <label class="">
                    Nacionalidad:
                  </label>
                  <br>

          					<select class="form-control m-select2" id="nacionalidades" name="nacionalidad_id">
                          <option value="0">Sin Seleccion</option>
          					    @foreach($Nacionalidades as $Nacionalidad)
                          @if($Nacionalidad->id == $paciente->nacionalidad_id)
                          <option value="{{$Nacionalidad->id}}" selected>{{$Nacionalidad->nombre}}</option>
                          @else
                          <option value="{{$Nacionalidad->id}}">{{$Nacionalidad->nombre}}</option>
                          @endif
                        @endforeach
          					</select>

                  <span class="m-form__help">
                    Introduce Nacionalidad
                  </span>
                </div>
              </div>
            </div>

          <!--end::Form-->
        </div>
        <!--end::Portlet-->
<!--begin::Portlet-->
        <div class="m-portlet">
          <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
              <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon m--hide">
                  <i class="la la-gear"></i>
                </span>
                <h3 class="m-portlet__head-text">
                  Contacto
                </h3>
              </div>
            </div>
          </div>
          <!--begin::Form-->

            <div class="m-portlet__body">
              <div class="form-group m-form__group row">
                <label class="col-lg-2 col-form-label">
                  Telefono Movil:
                </label>
                <div class="col-lg-3">
                  <input type="text" class="form-control m-input" name="movil" value="{{$paciente->movil}}" id="movil" placeholder="Introduce Telefono">

                </div>
                <label class="col-lg-2 col-form-label">
                  Telefono Fijo:
                </label>
                <div class="col-lg-3">
                  <input type="text" class="form-control m-input" name="fijo" value="{{$paciente->fijo}}" id="fijo" placeholder="Introduce Tu Telefono Fijo">

                </div>
              </div>
              <div class="form-group m-form__group row">
                <label class="col-lg-2 col-form-label">
                  Telefono Trabajo:
                </label>
                <div class="col-lg-3">
                  <div class="m-input-icon m-input-icon--right">
                    <input type="text" class="form-control m-input" name="trabajo"  id="trabajo" value="{{$paciente->trabajo}}" placeholder="Telefono de Trabajo">
                    <span class="m-input-icon__icon m-input-icon__icon--right">
                      <span>
                        <i class="la la-phone"></i>
                      </span>
                    </span>
                  </div>

                </div>
                <label class="col-lg-2 col-form-label">
                  Extension:
                </label>
                <div class="col-lg-3">
                  <div class="m-input-icon m-input-icon--right">
                    <input type="text" class="form-control m-input" name="extension" value="{{$paciente->extension}}" placeholder="Extension">
                    <span class="m-input-icon__icon m-input-icon__icon--right">
                      <span>
                        <i class="la la-phone"></i>
                      </span>
                    </span>
                  </div>

                </div>
              </div>
              <div class="form-group m-form__group row">
                <label class="col-lg-2 col-form-label">
                  Correo:
                </label>
                <div class="col-lg-6">
                  <div class="m-input-icon m-input-icon--right">
                    <input type="text" class="form-control m-input" name="correo"  value="{{$paciente->correo}}" placeholder="Introduce Correo">
                    <span class="m-input-icon__icon m-input-icon__icon--right">
                      <span>
                        <i class="la la-envelope"></i>
                      </span>
                    </span>
                  </div>

                </div>

                </div>
                <h6>Contacto para Emergencias</h6>
                <hr>
                <div class="form-group m-form__group row">
                  <div class="col-lg-4">
                    <label>
                      Nombre de Contacto:
                    </label>
                    <input name="contacto_nom"  type="text" class="form-control m-input" value="{{$paciente->contacto_nom}}" placeholder="Introduce el Nombre">

                  </div>
                  <div class="col-lg-4">
                    <label class="">
                      Apellido Paterno de Contacto:
                    </label>
                    <input name="contacto_app" type="text" class="form-control m-input" value="{{$paciente->contacto_app}}" placeholder="Introduce el Apellido Paterno">

                  </div>
                  <div class="col-lg-4">
                    <label>
                      Apellido Materno de Contacto:
                    </label>
                    <input name="contacto_apm" type="text" class="form-control m-input" value="{{$paciente->contacto_apm}}" placeholder="Introduce el Apellido Materno">

                  </div>
                  <div class="col-lg-12">
                    <br>
                  </div>
                  <div class="col-lg-4">
                    <label>
                      Fijo de Contacto:
                    </label>
                    <input name="contacto_fijo" type="text" class="form-control m-input" value="{{$paciente->contacto_fijo}}" placeholder="Introduce el Fijo de Contacto">

                  </div>
                  <div class="col-lg-4">
                    <label class="">
                     Movil de Contacto:
                    </label>
                    <input name="contacto_movil" type="text" class="form-control m-input" value="{{$paciente->contacto_movil}}" placeholder="Introduce el Movil de Contacto">

                  </div>
                  <div class="col-lg-4">
                    <label>
                      Parentesco:
                    </label>
                    <input name="parentesco" type="text" class="form-control m-input" value="{{$paciente->parentesco}}" placeholder="Introduce el parentesco con el paciente">

                  </div>
                </div>
            </div>
          <!--end::Form-->
        </div>
        <!--end::Portlet-->
<!--begin::Portlet-->
        <div class="m-portlet">
          <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
              <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon m--hide">
                  <i class="la la-gear"></i>
                </span>
                <h3 class="m-portlet__head-text">
                  Direccion
                </h3>
              </div>
            </div>
          </div>
          <!--begin::Form-->

            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                  <div class="col-lg-6">
                    <label>
                      Direccion:
                    </label>
                    <input type="text" name="direccion" class="form-control m-input" value="{{$paciente->direccion}}" placeholder="Calle, numero.. ">
                    <span class="m-form__help">

                    </span>
                  </div>
                  <div class="col-lg-6">
                    <label>
                      Provincia:
                    </label>

                      <select class="form-control m-select2" id="provincias" value="{{$paciente->provincia}}" name="provincia">
                            <option value="0">Sin Seleccion</option>

                      </select>

                    <span class="m-form__help">

                    </span>
                  </div>

                </div>
                <div class="form-group m-form__group row">
                  <div class="col-lg-6">
                    <label>
                      Ciudad:
                    </label>
                    <select class="form-control m-select2" id="ciudad" value="{{$paciente->ciudad}}" name="ciudad">
                          <option value="0">Sin Seleccion</option>

          					</select>
                  </div>
                  <div class="col-lg-6">
                    <label>
                      Sector:
                    </label>
                    <select class="form-control m-select2" id="sector" value="{{$paciente->sector}}" name="sector">
                          <option value="0">Sin Seleccion</option>

          					</select>

                  </div>

                </div>
          <!--end::Form-->
        </div>
        </div>
        <!--end::Portlet-->
<!--begin::Portlet-->
        <div class="m-portlet">
          <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
              <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon m--hide">
                  <i class="la la-gear"></i>
                </span>
                <h3 class="m-portlet__head-text">
                  Otros Datos
                </h3>
              </div>
            </div>
          </div>
          <!--begin::Form-->

            <div class="m-portlet__body">
              <div class="form-group m-form__group row">
                <div class="col-lg-6">
                  <label>
                    Ocupacion:
                  </label>
                  <input type="text" name="ocupacion" class="form-control m-input" value="{{$paciente->ocupacion}}" placeholder="Ocupacion ">
                  <span class="m-form__help">

                  </span>
                </div>
                <div class="col-lg-6">
                  <label>
                    Quien lo Requiere:
                  </label>
                  <input type="text" name="requerido" class="form-control m-input" value="{{$paciente->requerido}}" placeholder="Ocupacion ">
                  <span class="m-form__help">

                  </span>
                </div>


              </div>
              <div class="form-group m-form__group row">
                <div class="col-lg-6">
                  <label>
                    Escolaridad:
                  </label>

                    <select class="form-control m-select2" id="escolaridad" value="{{$paciente->escolaridad}}" name="escolaridad">
                          <option value="0">Ninguna</option>
                          <option value="1">Primaria</option>
                          <option value="2">Secundaria</option>
                          <option value="3">Universitaria</option>

                    </select>

                  <span class="m-form__help">

                  </span>
                </div>
                <div class="col-lg-6">
                  <label>
                    Estado Civil:
                  </label>
                  <select class="form-control m-select2" id="estado_civil" value="{{$paciente->estado_civil}}" name="estado_civil">
                        <option value="0">Soltero</option>
                        <option value="1">Casado</option>
                        <option value="2">Union Libre</option>
                  </select>
                </div>
                <div class="col-lg-6">
                  <label>
                    Acepta Transfusion?:
                  </label>
                  <div class="m-radio-inline">
                    <label class="m-radio m-radio--solid">
                      @if(old('transfusion') == 0)
                        <input type="radio" name="transfusion" checked value="0">
                      @else
                        <input type="radio" name="transfusion" value="0">
                      @endif
                      Si
                      <span></span>
                    </label>
                    <label class="m-radio m-radio--solid">
                      @if(old('transfusion') == 1)
                        <input type="radio" name="transfusion" checked value="1">
                      @else
                        <input type="radio" name="transfusion" value="1">
                      @endif
                      No
                      <span></span>
                    </label>
                  </div>
                  <span class="m-form__help">
                    El cliente esta dispuesto a la transfusion?
                  </span>
                </div>

              </div>
            </div>
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
              <div class="m-form__actions m-form__actions--solid">
                <div class="row">
                  <div class="col-lg-4"></div>
                  <div class="col-lg-4">
                    <button type="reset" class="btn btn-secondary">
                      Resetear
                    </button>

                    <button type="reset" class="btn btn-danger">
                      Cancelar
                    </button>

                    <button type="submit" class="btn btn-brand">
                      Guardar
                    </button>
                  </div>
                  <div class="col-lg-4"></div>
                </div>
                <br>
              </div>
            </div>
            {!! Form::close() !!}
          <!--end::Form-->
        </div>
        <!--end::Portlet-->
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script type="text/javascript" src="{{asset('custom_js/input_mask.js')}}"></script>
<script type="text/javascript">
$('#escolaridad').select2({
    placeholder: "Selecciona una Nacionalidad"
});
$('#estado_civil').select2({
    placeholder: "Selecciona una Nacionalidad"
});
$('#nacionalidades').select2({
    placeholder: "Selecciona una Nacionalidad"
});
$('#ciudad').select2({
    placeholder: "Selecciona una Nacionalidad"
});
$('#sector').select2({
    placeholder: "Selecciona una Nacionalidad"
});
$('#provincias').select2({
    placeholder: "Selecciona una Nacionalidad"
});
$('#m_datepicker_2').datepicker({
    todayHighlight: true,
    autoclose: true,
    pickerPosition: 'bottom-left',
    format: 'yyyy/mm/dd'
}).init;
</script>

@endsection
