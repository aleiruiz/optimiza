<div class="modal fade" id="m_datetimepicker_modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Crear un nuevo evento</h5>
				<button type="reset" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true" class="la la-remove"></span>
				</button>
			</div>
			<div class="alert alert-danger print-error-msg" style="display:none">
        <ul></ul>
    </div>
			<form class="m-form m-form--fit m-form--label-align-right" id="Evento">
				<div class="modal-body">
					<div class="form-group m-form__group row m--margin-top-20">
						<label class="col-form-label col-lg-3 col-sm-12">Asunto</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input type="text" class="form-control" name="asunto" data-z-index="1100" placeholder="Asunto"/>
						</div>
					</div>
          <div class="form-group m-form__group row m--margin-top-20">
						<label class="col-form-label col-lg-3 col-sm-12">Descripcion</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input type="text" class="form-control" name="descripcion" data-z-index="1100" placeholder="Descripcion breve"/>
						</div>
					</div>
          <div class="form-group m-form__group row m--margin-bottom-20">
           <label class="col-form-label col-lg-3 col-sm-12">Fecha y Hora de Inicio</label>
           <div class="col-lg-9 col-md-9 col-sm-12">
             <div class="input-group date"  data-z-index="1100">
               <input type="text" class="form-control m-input" name="fecha_inicio" readonly  value="{{$hoy}}" id="m_datetimepicker_3_modal"/>
               <div class="input-group-append">
                 <span class="input-group-text">
                 <i class="la la-calendar-plus-o glyphicon-th"></i>
                 </span>
               </div>
             </div>
           </div>
         </div>
					<div class="form-group m-form__group row m--margin-bottom-20">
						<label class="col-form-label col-lg-3 col-sm-12">Fecha y Hora de Fin</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<div class="input-group date"  data-z-index="1100">
								<input type="text" class="form-control m-input" readonly name="fecha_fin" value="{{$hoy}}" id="m_datetimepicker_2_modal"/>
								<div class="input-group-append">
									<span class="input-group-text">
									<i class="la la-calendar-plus-o glyphicon-th"></i>
									</span>
								</div>
							</div>
							<span class="m-form__help">Las fechas de inicio y fin deben ser validas para crear el registro</span>
						</div>
					</div>
					{!! Form::hidden('_token', Session::token(), ['class' => 'form-control']) !!}
				</div>
				<div class="modal-footer">
					<button type="reset" class="btn btn-brand m-btn" data-dismiss="modal">Cerrar</button>
					<button type="reset" class="btn btn-success m-btn" onclick="crearEvento()">Crear</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="modal fade" id="showEvento" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Editar Evento</h5>
				<button type="reset" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true" class="la la-remove"></span>
				</button>
			</div>
			<div class="alert alert-danger print-error-msg" style="display:none">
        <ul></ul>
    </div>
			<form class="m-form m-form--fit m-form--label-align-right" id="formEvento">

			</form>
		</div>
	</div>
</div>
